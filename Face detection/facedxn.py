import cv2
import sys
import urllib.request
import numpy as np

imgpath=str(sys.argv[1])


def getImagefromURL(url=None, path=None):
	if url is not None:

		with urllib.request.urlopen(url) as resp:
		#resp=urllib.urlopen(url)
			data=resp.read();
		imagedata=np.asarray(bytearray(data), dtype="uint8")
		imagedata=cv2.imdecode(imagedata, cv2.IMREAD_COLOR)
		return imagedata

	elif path is not None:
		imagedata=cv2.imread(imgpath)
		return imagedata




face_cascade=cv2.CascadeClassifier('opencv-files/haarcascade_frontalface_alt.xml')

image=getImagefromURL(url=imgpath)

grayimg=cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)

faces=face_cascade.detectMultiScale(grayimg,1.2,6)
for (x,y,a,b) in faces:
    cv2.rectangle(image,(x,y),(x+a,y+b), (0,0,255), 8)

cv2.namedWindow('Pic', cv2.WINDOW_NORMAL)
#imgresize=cv2.resize(image, (1152, 864)) 


cv2.imshow('Pic', image)



cv2.waitKey(0)
cv2.destroyAllWindows()